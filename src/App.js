import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import Headersec from './components/header.js';
import Home from './components/home.js';
import Destpagesec from './components/singledestination/destinationpage.js';
import Dests from './components/singledestination/singledestinationpage.js'
import Tourpagesec from './components/singletours/tourpage.js';
import Tours from './components/singletours/singletourpage.js';
import Aboutctnt from './components/about.js';
import Hotelpagesec from './components/singlehotels/hotelpage.js';
import Hotels from './components/singlehotels/singlehotelpage.js';
import contactctnt from './components/contact.js';
import Cabpagesec from './components/singlecab/cabpage.js';
import Cars from './components/singlecab/singlecabpage.js';
import Fotersec from './components/footer.js';
import Tourname from './components/singletour.js';
import NotFound from './components/notfound.js';
//import cabData from './components/singlecab/cabdata.js';


class App extends Component {

  render() {
    return (
    <Router>
      <div className="App">
        <Headersec />
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/destinations" component={Destpagesec} exact />
          <Route path="/destination/:id" component={Dests} />
          <Route path="/tours" component={Tourpagesec} exact />
          <Route path="/tour/:id" component={Tours} />
          <Route path="/about" component={Aboutctnt} />
          <Route path="/hotels" component={Hotelpagesec} exact/>
          <Route path="/hotel/:id" component={Hotels} />
          <Route path="/contact" component={contactctnt} />
          <Route path="/vehciles" component={Cabpagesec} exact />
          <Route path={`/vehcile/:id`} component={Cars} />
          <Route path="/singletour" component={Tourname} />
          <Route path="/singletour" component={Tourname} />
          <Route component = {NotFound} />
          </Switch>
          <Fotersec />
      </div>
    </Router>
    );

  }
  }

export default App;
