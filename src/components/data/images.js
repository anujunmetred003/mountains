import React from 'react';
import Cardata from '../singlecab/data.json';

    const rendCar = (props) => {
        let acar = Cardata.cabs.map(cab => <a href ={`/cab/${cab.id}`} class="btn"> BOOK NOW </a> )
        return acar
    }


 var Data = 
                {

                topbarconatact : [
                    {
                        telimg: "./images/phone.png",
                        telno: "+91 1234567890",
                        tellink: "tel:+91-123456789"
                    }
                ],
            
                dsktoplogo :[
                    {
                        logoimg: "./images/logo.png",
                        logolink: "/"
                    }
                ],
            
                leftmenu: [
                    {
                        menulink: "/destination",
                        menuimg: "./images/loc.png",
                        menutxt: "DESTINATIONS"
                    },
                    {
                        menulink: "/hotel",
                        menuimg: "./images/hotel.png",
                        menutxt: "HOTELS"
                    },
                    {
                        menulink: "/vehciles",
                        menuimg: "./images/cabs.png",
                        menutxt: "CABS"
                    }
                ],
            
                rightmenu: [
                    {
                        menulink: "/tour",
                        menuimg: "./images/tour.png",
                        menutxt: "TOURS"
                    },
                    {
                        menulink: "/about",
                        menuimg: "./images/about.png",
                        menutxt: "ABOUT"
                    },
                    {
                        menulink: "/contact",
                        menuimg: "./images/contact.png",
                        menutxt: "CONTACT"
                    }
                ],
            
            
                
                
                tourowldata : [
                    
                    {
                                tourimg: "./images/kullu.png",    
                                title: "Kullu",
                                discription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed",
                                type: "ADVENRURE"
                    },
                    {
                                tourimg: "./images/dshala.png",   
                                title: "Dahramshala",
                                discription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed",
                                type: "FAMILY"
                    },
                    {
                                tourimg: "./images/shimla.png", 
                                title: "Shimla",
                                discription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed",
                                type: "DESTINATION"
                    },
                       {
                                tourimg: "./images/manali.png", 
                                title: "Manali",
                                discription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed",
                                type: "ADVENRURE"
                    },
                       {
                                tourimg: "./images/palmpur.png", 
                                title: "Palampur",
                                discription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed",
                                type: "FAMILY"
                    },
                    {
                        tourimg: "./images/dshala.png",   
                        title: "Dahramshala",
                        discription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed",
                        type: "FAMILY"
            }
                ],
            
            
                
             hoteldata : [
                    
                    {
                                id: 1,
                                hotelimg: "./images/room1.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                           
                    },
                    {
                                id: 2,
                                hotelimg: "./images/room2.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                    },
                    {
                        id: 3,
                                hotelimg: "./images/room3.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                    },
                     {
                        id: 4,
                                hotelimg: "./images/room4.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                    },
                     {
                        id: 5,
                                hotelimg: "./images/room1.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                    },
                    {
                        id: 6,
                                hotelimg: "./images/room2.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                    id: 7,
                                hotelimg: "./images/room3.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                    id: 8,
                                hotelimg: "./images/room4.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                    id: 9,
                                hotelimg: "./images/room1.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                    id: 10,
                                hotelimg: "./images/room2.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                    id: 12,
                                hotelimg: "./images/room3.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                    id: 13,
                                hotelimg: "./images/room4.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   }
                ],
            
            
            
                hotelloaddata : [
                    
                    {
                       
                                hotelimg: "./images/room1.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                           
                    },
                    {
                                hotelimg: "./images/room2.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                    },
                    {
                                hotelimg: "./images/room3.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                    },
                     {
                                hotelimg: "./images/room4.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                    },
                     {
                                hotelimg: "./images/room1.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                    },
                    {
                                hotelimg: "./images/room2.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                                hotelimg: "./images/room3.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                                hotelimg: "./images/room4.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                                hotelimg: "./images/room1.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                                hotelimg: "./images/room2.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                                hotelimg: "./images/room3.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   },
                   {
                                hotelimg: "./images/room4.png",    
                                hoteltitle: "HOTEL NAME",
                                hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
                                hotelurl: "/singlehotel"
                   }
                ],
            
            
            
                
             advendata : [
                    
                    {
                                adventrhedng: "Let's go on a",    
                                adventrdnhedng: "ADVENTURE",
                                raftingimg: "./images/rafting.png",
                                playimg: "./images/play.png",
                                playurl: "https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                    }   
                ],
                
                chosedata : [
                    
                    {
                                choseimg: "./images/cam.png",    
                                chosetitle: "SIGHTSEEING",
                                chosediscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print"
                    },
                    {
                                choseimg: "./images/destination.png",    
                                chosetitle: "RESTURANTS",
                                chosediscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print"
                    },
                    {
                                choseimg: "./images/erth.png",    
                                chosetitle: "HOTELS",
                                chosediscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print"
                    },
                    {
                                choseimg: "./images/hnds.png",    
                                chosetitle: "SUPPORT",
                                chosediscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print"
                    }
            
                    
                ],
            
                aboutdata : [
                    {
                        aboutpra: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
                        aboutimage: "./images/Khajjiar.jpg"
             }
            
                ],
            
            
                alltourdata : [
                 {
                                destname: "SHIMLA"         
                 },
                      {
                                destname: "MANALI" 
                 },
                      {
                                destname: "DHARAMSHALA"
                 },
                      {
                                destname: "PALAMPUR"
                 }
                 ],
                
                
                    shimladetail : [
                    
                    {
                                
                                alltourimg: "./images/shimla1.png",    
                                alltourtitle: "SHIMLA - MANALI 3N/4D",
                                alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                                shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                                shimlatoururl: "/singletour"
                    },
                            {
                                
                                alltourimg: "./images/shilma2.png",    
                                alltourtitle: "SHIMLA - MANALI 3N/4D",
                                alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                                shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                                shimlatoururl: "/singletour"
                                
                    },
                            {
                                alltourimg: "./images/shimla3.png",    
                                alltourtitle: "SHIMLA - MANALI 3N/4D",
                                alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                                shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                                shimlatoururl: "/singletour"
                    },
                            {
                                alltourimg: "./images/shimla4.png",    
                                alltourtitle: "SHIMLA - MANALI 3N/4D",
                                alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                                shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                                shimlatoururl: "/singletour"
                    },
                            {
                                alltourimg: "./images/shimla1.png",    
                                alltourtitle: "SHIMLA - MANALI 3N/4D",
                                alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                                shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                                shimlatoururl: "/singletour"
                    }
                ],
                
                    manalidetail : [
                    
                        {
                                
                            alltourimg: "./images/shimla1.png",    
                            alltourtitle: "SHIMLA - MANALI 3N/4D",
                            alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                            shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                            shimlatoururl: "/singletour"
                    },
                        {
                            
                            alltourimg: "./images/shilma2.png",    
                            alltourtitle: "SHIMLA - MANALI 3N/4D",
                            alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                            shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                            shimlatoururl: "/singletour"
                   },
                        {
                            alltourimg: "./images/shimla3.png",    
                            alltourtitle: "SHIMLA - MANALI 3N/4D",
                            alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                            shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                            shimlatoururl: "/singletour"
                   },
                        {
                            alltourimg: "./images/shimla4.png",    
                            alltourtitle: "SHIMLA - MANALI 3N/4D",
                            lltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                            shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                            shimlatoururl: "/singletour"
                   },
                        {
                            alltourimg: "./images/shimla1.png",    
                            alltourtitle: "SHIMLA - MANALI 3N/4D",
                            alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                            shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                            shimlatoururl: "/singletour"
                  }
                ],
                dharamshaladetail : [
                    
                    {
                                
                        alltourimg: "./images/shimla1.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                 },
                    {
                        
                        alltourimg: "./images/shilma2.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                },
                    {
                        alltourimg: "./images/shimla3.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                 },
                    {
                        alltourimg: "./images/shimla4.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                },
                    {
                        alltourimg: "./images/shimla1.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                  }
                ],
            
                palampurdetail : [
                    
                    {
                                
                        alltourimg: "./images/shimla1.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                },
                    {
                        
                        alltourimg: "./images/shilma2.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                },
                    {
                        alltourimg: "./images/shimla3.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                },
                    {
                        alltourimg: "./images/shimla4.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                },
                    {
                        alltourimg: "./images/shimla1.png",    
                        alltourtitle: "SHIMLA - MANALI 3N/4D",
                        alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
                        shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
                        shimlatoururl: "/singletour"
                }
                ],
            
                reviewdata : [
                    
                    {
                                
                        reviewimg: "./images/face.png",    
                        reviewtitle: "What people says",
                        reviewsectitle: "TRAVELLER REVIEWS",
                        reviewdiscription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
                },
                {
                            
                    reviewimg: "./images/face.png",    
                    reviewtitle: "What people says",
                    reviewsectitle: "TRAVELLER REVIEWS",
                    reviewdiscription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
            },
            {
                            
                reviewimg: "./images/face.png",    
                reviewtitle: "What people says",
                reviewsectitle: "TRAVELLER REVIEWS",
                reviewdiscription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
            }
               ],
            
            
            destinationdata : [
                    
                {
                            
                    destpageimg: "./images/dshala.png",    
                    destpageimgtitle: "Dharamshala",
                    destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                    destpageimgsuit: "FAMILY",
                    url: "/dharamshala"
            },
            
            {
                            
                destpageimg: "./images/kullu.png",    
                destpageimgtitle: "Kullu",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/kullu"
            },
            
            {
                            
                destpageimg: "./images/shimla.png",    
                destpageimgtitle: "Shimla",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/shimla"
            },
            
            {
                            
                destpageimg: "./images/manali.png",    
                destpageimgtitle: "Manali",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/kullu"
            },
            
            {
                            
                destpageimg: "./images/dshala.png",    
                destpageimgtitle: "Dharamshala",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/dharamshala"
            },
            
            {
                            
                destpageimg: "./images/palmpur.png",    
                destpageimgtitle: "Palampur",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/kullu"
            }
            ],
            
            destinationloaddata : [
            {
                            
                destpageimg: "./images/dshala.png",    
                destpageimgtitle: "Dharamshala",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/dharamshala"
            },
            
            {
                            
                destpageimg: "./images/manali.png",    
                destpageimgtitle: "Manali",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/kullu"
            },
            
            {
                            
                destpageimg: "./images/dshala.png",    
                destpageimgtitle: "Dharamshala",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/dharamshala"
            },
            {
                            
                destpageimg: "./images/shimla.png",    
                destpageimgtitle: "Shimla",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/shimla"
            },
            
            {
                            
                destpageimg: "./images/dshala.png",    
                destpageimgtitle: "Dharamshala",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/dharamshala"
            },
            
            {
                            
                destpageimg: "./images/palmpur.png",    
                destpageimgtitle: "Palampur",
                destpageimgpera: "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print.",
                destpageimgsuit: "FAMILY",
                url: "/kullu"
            }
            ],
            
            contactdata: [
            
                {
                    contactpera: "ADDRESS",
                    contactheading: "YOUR ADDRESS COMES HERE",
                    contactaddrs: "#123, ENJFHJD, SDJGMD"
                },
                {
                    contactpera: "EMAIL",
                    contactheading: "contact@mail.com",
                    contatclnk: "mailto:contact@mail.com"
                },
                {
                    contactpera: "PHONE",
                    contactheading: "1800 123 4569",
                    contatclnk: "tel:1800-123-4569"
                }
            ],
            
            cabdata: [
                {
                id : 1,    
                cabimage: "./images/cab-1.jpg",
                cabname: "INDIGO",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 2,
                cabimage: "./images/cab-2.jpg",
                cabname: "INNOVA",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 3,
                cabimage: "./images/cab-3.jpg",
                cabname: "ERTICA",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 4,
                cabimage: "./images/cab-4.jpg",
                cabname: "SWIFT DESIRE",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 5,
                cabimage: "./images/cab-5.jpg",
                cabname: "TRAVELLER",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 6,
                cabimage: "./images/cab-6.jpg",
                cabname: "ALTO 800",
                cabprice: "8000 INR",
                caburl: rendCar()
            },
            {
                id : 7,
                cabimage: "./images/cab-7.jpg",
                cabname: "WAGON - R",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 8,
                cabimage: "./images/cab-8.jpg",
                cabname: "SWIFT DESIRE",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 9,
                cabimage: "./images/cab-6.jpg",
                cabname: "ALTO 800",
                cabprice: "8000 INR",
                caburl: rendCar()
            },
            {
                id : 10,
                cabimage: "./images/cab-2.jpg",
                cabname: "INNOVA",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 11,
                cabimage: "./images/cab-3.jpg",
                cabname: "ERTICA",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 12,
                cabimage: "./images/cab-8.jpg",
                cabname: "SWIFT DESIRE",
                cabprice: "12000 INR",
                caburl: rendCar()
            }
              
            ],
            
            cabloaddata: [
                {
                id : 13,
                cabimage: "./images/cab-1.jpg",
                cabname: "INDIGO",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 14,
                cabimage: "./images/cab-2.jpg",
                cabname: "INNOVA",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 15,
                cabimage: "./images/cab-3.jpg",
                cabname: "ERTICA",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 16,
                cabimage: "./images/cab-4.jpg",
                cabname: "SWIFT DESIRE",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 17,
                cabimage: "./images/cab-5.jpg",
                cabname: "TRAVELLER",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 18,
                cabimage: "./images/cab-6.jpg",
                cabname: "ALTO 800",
                cabprice: "8000 INR",
                caburl: rendCar()
            },
            {
                id : 19,
                cabimage: "./images/cab-7.jpg",
                cabname: "WAGON - R",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 20,
                cabimage: "./images/cab-8.jpg",
                cabname: "SWIFT DESIRE",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 21,
                cabimage: "./images/cab-6.jpg",
                cabname: "ALTO 800",
                cabprice: "8000 INR",
                caburl: rendCar()
            },
            {
                id : 22,
                cabimage: "./images/cab-2.jpg",
                cabname: "INNOVA",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 23,
                cabimage: "./images/cab-3.jpg",
                cabname: "ERTICA",
                cabprice: "12000 INR",
                caburl: rendCar()
            },
            {
                id : 24,
                cabimage: "./images/cab-8.jpg",
                cabname: "SWIFT DESIRE",
                cabprice: "12000 INR",
                caburl: rendCar()
            }
              
            ]
            
            }
    
export default Data;