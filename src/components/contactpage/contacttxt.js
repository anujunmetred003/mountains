import React from "react";
import axios from "axios";
import * as emailjs from 'emailjs-com'
import Data from '../data/images.json'


const TITLE = 'CONTACT US'

    class Contactpagesec extends React.Component {
        constructor (props){
            super(props);
          
            this.state = {
              acf: {},
            };
          }
          componentDidMount() {
            return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/189`)
            .then((response) => response.json())
            .then((responseJson) => {
              const { acf } =  responseJson;
              this.setState({ acf });       
            })
            .catch((error) => {
              console.error(error);
            });  
        }  

      state = {
             firstname: '',
             lastname: '',
             email: '',
             phone: '',
             enquiry: '',
             message: '',
         }  
         
         handleSubmit(e) {
          e.preventDefault()
          const { firstname, lastname, email, phone, enquiry, message } = this.state
          let templateParams = {
            from_name: email,
            to_name: 'anujverma09713@gmail.com',
            firstname: firstname,
            lastname: lastname,
            phone: phone,
            enquiry: enquiry,
            message_html: message,
           }

           emailjs.send(
            'gmail',
            'template_XXXXXXXX',
             templateParams,
            'user_XXXXXXXXXXXXXXXXXXXX'
           )
           this.resetForm()
       }

      resetForm(){
    
        this.setState({firstname: '', lastname: '', email: '', phone: '', enquiry: '', message: '' })
     }

     handleChange = (param, e) => {
      this.setState({ [param]: e.target.value })
    }

render(){
    return(
<div>
<div className="inner-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
      </div>
<div className="contact-sec">
    <div className="container mb-3">
        <div className="row">
            <div className="col-md-4 col-12">
                <div className="contact-txt">
                    <p>ADDRESS</p>
                        <h3>{this.state.acf.address}</h3>
                </div>
            </div>
             <div className="col-md-4 col-12">
                <div className="contact-txt">
                    <p>EMAIL</p>
                    <a href={`mailto:${this.state.acf.email}`}><h3>{this.state.acf.email}</h3></a>
                </div>
             </div>           
            <div className="col-md-4 col-12">
                <div className="contact-txt">
                    <p>PHONE</p>
                    <a href={`tel:${this.state.acf.phone}`}><h3>{this.state.acf.phone}</h3></a>
                </div>
            </div>     
        </div>
    </div>
    <div className="contact-field-sec">
        <div className="container">
            <div className="row">
                <div className="col-md-6 col-12">
                    <div className="contact-bar">
                        <p>All*Marked fiels are compulsory</p>
                <form method="POST" onSubmit={this.handleSubmit.bind(this)} class="form-inlin">
                    <div className="col-12">
                            <div className="row">
                        <div className="col-md-6 col-12 px-0">                     
                              <div class="form-group">
                                <input type="text" id="firstname"  value={this.state.firstname} onChange={this.handleChange.bind(this, 'firstname')} class="form-control name-fields" placeholder="First name*" required />
                              </div>
                        </div>
                        <div className="col-md-6 col-12 px-0">
                              <div class="form-group">
                                <input type="text" id="lastname"  value={this.state.lastname} onChange={this.handleChange.bind(this, 'lastname')} class="form-control lastname-fields" placeholder="Last name*" required />
                              </div>
                            </div>
                        </div>
                      </div>
                    <div class="form-group">
                        <input type="text" id="email"  aria-describedby="emailHelp" value={this.state.email} onChange={this.handleChange.bind(this, 'email')} class="form-control field" placeholder="Email" required />
                    </div>
                    <div class="form-group">
                        <input type="text" id="phone" value={this.state.phone} onChange={this.handleChange.bind(this, 'phone')} class="form-control field" placeholder="Phone no.*" required />
                    </div>
                    <div class="form-group">
                        <input type="text" id="enquiry" value={this.state.enquiry} onChange={this.handleChange.bind(this, 'enquiry')} class="form-control field" placeholder="Enquiry*" required />
                    </div>
                    <div class="form-group">
                        <textarea id="message" value={this.state.message} onChange={this.handleChange.bind(this, 'message')} class="form-control msg-field" placeholder="Message*"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="message-btn btn-block btn-lg">SUBMIT</button>
                    </div>
                  </form>
                    </div>
                </div>
                <div className="col-md-6 col-12 map_bar">
                    <div className="map-sec">
                    <iframe src={this.state.acf.map} width="820" height="560" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
);
}

}   

export default Contactpagesec;