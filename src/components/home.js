import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
  } from 'react-router-dom';
import Frstsec from './homepage/baner.js';
import Scndsec from './homepage/hotel.js';
import Thrdsec from './homepage/adventure.js';
import Frthsec from './homepage/playsec.js';
import Chosesec from './homepage/choose.js';
import Alltoursec from './homepage/alltours.js';
import Reviwesec from './homepage/review.js';



    const Home = () => (

     
        <div>
        
        <Frstsec />
        <Scndsec /> 
        <Thrdsec /> 
        <Frthsec /> 
        <Chosesec />
        <Router>
        <Switch>
        <Route component={Alltoursec} exact /> 
        </Switch>
        </Router>
        <Reviwesec />
        
        
        
        </div>
    );
  
  
  export default Home;


