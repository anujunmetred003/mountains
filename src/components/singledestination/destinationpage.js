
import React, { Component } from "react";
import destData from './destinationdata.js';
import { Link } from "react-router-dom";


const TITLE = "DESTINATIONS"


class Destpagesec extends Component{

  constructor(props){
    super(props);

    this.state = {
      destinations: [],
      dataRoute: 'https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/destination'
    }
  }
  render(){
    return(
      <div>
      <div className="inner-cab-banner">
                <div className="container text-center">
                    <h1 className="inner-heading">{ TITLE }</h1>
                </div>
      </div>
      <div className="more-destination">
          <div className="container destion-wdth">
              <div className="row">
                {this.state.destinations.map((dest)  => 
              <div key={'dest-${dest.id}}'} className="col-md-4 col-12 dest_sec mb-2">
                <Link to={`/destination/${dest.id}`} className="dest_link">
                <img src={dest.images} className="img-fluid" />
           <div className="destinton-txt">
              <h3>{dest.name}</h3>
                <p>{dest.description}</p>
                <p className="dest_item_vlg">{dest.suit}</p>  
            </div>
            </Link>
            </div>
            )} 
            </div>
              </div>
          </div>
          </div>
    );
  }
    componentDidMount(){
      fetch(this.state.dataRoute)
        .then(res => res.json())
        .then(destinations => this.setState((prevState, props) => {
          return { destinations: destinations.map(this.mapdest)};
        }));
    }
  
    mapdest(dest){
      return {
        id: dest.id,
        images: dest.acf.image,
        name: dest.acf.title,
        description: dest.acf.short_discription,
        suit: dest.acf.suit
      }
    }

  }

export default Destpagesec;


