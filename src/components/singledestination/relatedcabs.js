import React, { Component } from 'react';
import destData from './destinationdata.js';
import ReactShadowScroll from 'react-shadow-scroll';



class Relatedcabs extends Component {
    
    render(){
    
    const Relate = () => {
        // const id = match.params.id;
        // const cab = cabData.find(cab => cab.id == id);

    return(
    <div className="scrol_sec">
        <ReactShadowScroll style={{ height: 350 }} id="srcol">
        {destData.map(more =>
            <a href={`/destination/${more.id}`}>
           <div className="ww">
              <img className="img_icons" src={more.destpageimg} />
              <h5>{more.destpageimgtitle}</h5>
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <p>{more.destpageimgsuit}</p>
           </div>
           </a>  
        )}
        </ReactShadowScroll>
      </div>
        )
        }

        return(
            <div>
                { Relate() }
            </div>
        )
        }
}

export default Relatedcabs;