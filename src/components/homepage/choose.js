import React, { Component } from "react";
import axios from 'axios';
import First from './choosedata/first.js';
import Second from './choosedata/second.js';
import Third from './choosedata/third.js';
import Four from './choosedata/four.js';

 class Chosesec extends Component {
  constructor (props){
    super(props);
  
    this.state = {
      acf: {},
    };
  }
  componentDidMount() {
    return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/84`)
    .then((response) => response.json())
    .then((responseJson) => {
      const { acf } =  responseJson;
      this.setState({ acf });       
    })
    .catch((error) => {
      console.error(error);
    });  
}
  render() {
  return(
      <div className="why-sec">
         <div className="container text-center">
         <h4 className="why-pera">{this.state.acf.choose_heading}</h4>
         <h3 className="why-heading">{this.state.acf.choose_sub_heading}</h3>     
           <div className="row">
            <First />
            <Second />
            <Third />
            <Four /> 
          </div>
      </div>
    </div>
        )
      } 
    }
export default Chosesec;