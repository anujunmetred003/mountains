import React, { Component } from "react";
import Carousel from 'react-bootstrap/Carousel' ;


export class Reviewslider extends Component{
    constructor() {
        super();
        this.state = {
            customerreview: []
        }
      }
    
      componentDidMount() {
        let dataURL = "https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/testimonial?_embed";
        fetch(dataURL)
          .then(res => res.json())
          .then(res => {
            this.setState({
                customerreview: res
            })
            })
      }
    render(){
        return(
        <div className="bd-example">
            <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">
               <Carousel>
               
               {this.state.customerreview.map((review) => (
                  <Carousel.Item key={review.id}>
                        <img src={review.acf.user_image} className="rew-img" alt="face-img" />
                        <p className="rew-pera">{review.acf.user_review}</p>
                        <h5 className="user-title">{review.acf.user_name}</h5>
                   </Carousel.Item>
                ))}
               
               </ Carousel>
            </div>
         </div>

        )
    }
}
export default Reviewslider;