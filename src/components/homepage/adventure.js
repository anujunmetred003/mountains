import React, { Component } from "react";
import AOS from 'aos';

class Thrdsec extends Component{
   constructor (props){
      super(props);
    
      this.state = {
        acf: {},
      };
    }
    componentDidMount() {
      return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/84`)
      .then((response) => response.json())
      .then((responseJson) => {
        const { acf } =  responseJson;
        this.setState({ acf });       
      })
      .catch((error) => {
        console.error(error);
      });  
  }
render() {
return(

    <div>
         <div className="bnnr-img" data-aos="fade-up">
         <div className="container text-center">
         <h3 className="bnnr-heading">{this.state.acf.heading}</h3>
         <h1 className="bnnr-dn-heading">{this.state.acf.sun_heading}</h1>
         </div>
      </div>
    </div>
      )
   }
}
export default Thrdsec;





