import React, { Component } from "react";

 class Four extends Component{

  constructor (props){
    super(props);
  
    this.state = {
      acf: {},
    };
  }
  componentDidMount() {
    return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/84`)
    .then((response) => response.json())
    .then((responseJson) => {
      const { acf } =  responseJson;
      this.setState({ acf });       
    })
    .catch((error) => {
      console.error(error);
    });  
}
  render(){
     return(
      <div className="col-md-3 col-12">
      <div className="feature-bar text-center">
        <img src={this.state.acf.support_icon} className="featr-img" />
        <h5 className="featr-heading">{this.state.acf.support_heading}</h5>
        <p className="featre-pera">{this.state.acf.support_sub_text}</p>
      </div>
      </div>
      );
    }
    }

export default Four;





