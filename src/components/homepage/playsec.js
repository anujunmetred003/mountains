import React, { Component } from "react";
import axios from 'axios';

class Frthsec extends Component{
  constructor (props){
    super(props);
  
    this.state = {
      acf: {},
    };
  }
  componentDidMount() {
    return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/84`)
    .then((response) => response.json())
    .then((responseJson) => {
      const { acf } =  responseJson;
      this.setState({ acf });       
    })
    .catch((error) => {
      console.error(error);
    });  
}
render(){
  
    return(
    <div>
        <div className="container position-relative play-sec">
        <iframe width="936" height="459" src={this.state.acf.adventure_video} allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>         
    </div>
    )
  }
}
export default Frthsec;

