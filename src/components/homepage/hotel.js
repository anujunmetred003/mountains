import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import hotelData from '../singlehotels/hoteldata.js';

  class Scndsec extends Component{

      constructor() {
      super();
      this.state = {
        hotels: []
      }
    }
  
    componentDidMount() {
      let dataURL = "https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/hotel?_embed";
      fetch(dataURL)
        .then(res => res.json())
        .then(res => {
          this.setState({
            hotels: res
          })
        })
    }

    render(){
      var settings = {
      dots: false,
      autoplay: true,
      infinite: true,
      arrows: false,
      speed: 500,
      slidesToShow: 5 ,
      slidesToScroll: 5,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
            margin: 10,
            infinite: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
      };
      return(

    <div>
      <div className="hotel-sec" id="htl-sec">
         <div className="container text-center hotel-contain">
            <div className="hotel-heading-sec">
               <h2 className="hotel-heading">Hotels</h2>
               <p className="hotel-pera">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic</p>
            </div>
      
      <Slider {...settings}>
        {this.state.hotels.map(room => 
     <div className="item no-border" key={'room-${room.id}}'}>
                  <img src={room.acf.image} className="dest-img img-fluid" />
                  <div className="htl-cntnt">
                     <h3 className="htl-heading">{room.acf.hotel_name}</h3>
                     <p className="htl-pera">{room.acf.hotel_description}</p>
                     <a href={`/hotel/${room.id}`} className="btn ex-btn">EXPLORE</a>
               </div>
      </div>
      )}
    </Slider>

          
      </div>
      </div>
    
     
    </div>
     )
    }

  }
 


export default Scndsec;





