import React from "react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
//import Data from '../data/images.json'; 
import Tourdata from '../singletours/tourdata.js';
import Tourtabdata from './tour-tab-data.js';


   const Tourtab = ({ match, props }) => {

      const id = this.props.match.params.id;
      const dataitem = Tourdata.find(dataitem => dataitem.id == id);

    return(  

               <Tabs>
                     <TabList>
                     {Tourdata.map(tourtab => 
                        <Tab key={tourtab.id}>{tourtab.tourname}</Tab>
                      )}    
                     </TabList>
                     <Tourtabdata />
                  </Tabs>
      )  };
        
export default Tourtab;