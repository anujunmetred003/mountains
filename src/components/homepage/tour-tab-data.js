import React from "react";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
//import Data from '../data/images.json'; 
import Tourdata from '../singletours/tourdata.js';

const options = {
   margin: 10,
   responsiveClass: true,
   nav: true,
   dots: false,
   autoplay: false,
   smartSpeed: 1000,
   responsive: {
       0: {
           items: 1,
       },
       400: {
           items: 1,
       },
       600: {
           items: 2,
       },
       700: {
           items: 3,
       },
       1000: {
           items: 4,
 
       }
   },
 };



   const Tourtabdata = ({ match }) => {

      const id = match.params.id;
      const dataitem = Tourdata.find(dataitem => dataitem.tour.id == id);

    return(  

                     <div>
                     {dataitem.tour.map(tr => (
                     <TabPanel key={tr.id}>                     
                     <OwlCarousel 
                     className="owl-theme hotel-slider tab-slide" id="tab-slider"
                     {...options}
                     >-
                     
                     {dataitem.tour.map(tritem => (
                           <div className="item no-brdr">
                              <img src={tritem.alltourimg} className="tour-img" />
                              <div className="tour-cntnt">
                                 <h5 className="tours-heading">{tritem.urlname}</h5>
                                 <p className="tour-pera">{tritem.alltourdiscription}</p>
                              </div>
                           </div>
                           ))}
                              
                           </OwlCarousel>
                           </TabPanel>
                            ))}
                           </div>
      )  };
        
export default Tourtabdata;