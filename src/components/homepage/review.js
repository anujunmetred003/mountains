import React, {Component} from "react";
import Carousel from 'react-bootstrap/Carousel' 
import Data from '../data/images.json';
import Review from './review-slider.js';

export class Reviwesec extends Component {
   constructor (props){
      super(props);
    
      this.state = {
        acf: {},
      };
    }
    componentDidMount() {
      return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/84`)
      .then((response) => response.json())
      .then((responseJson) => {
        const { acf } =  responseJson;
        this.setState({ acf });       
      })
      .catch((error) => {
        console.error(error);
      });  
  }
   render(){
   return(

      <div>
      <div className="review-sec">
      <div className="container text-center rew-sec">
      <h5 className="rew-heading">{this.state.acf.review_heading}</h5>
      <h2 className="rew-dn-heading">{this.state.acf.review_subheading}</h2>
      <Review />
      </div>
      </div>
      </div>
   )
   }
   }




export default Reviwesec;