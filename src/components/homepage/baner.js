import React, { Component } from "react";
import AOS from 'aos';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import destData from '../singledestination/destinationdata.js';
import axios from 'axios';

  class Frstsec extends Component{

    constructor() {
      super();
      this.state = {
        dests: []
      }
    }
  
    componentDidMount() {
      let dataURL = "https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/destination?_embed";
      fetch(dataURL)
        .then(res => res.json())
        .then(res => {
          this.setState({
            dests: res
          })
        })
    }
    
    render()
    {
      var settings = {
      dots: false,
      infinite: true,
      autoplay: true,
      arrows: true,
      speed: 500,
      slidesToShow: 5 ,
      slidesToScroll: 5,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
            margin: 10,
            infinite: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
      };

    return(

        <div>
        <div className="tour-sec" data-aos="fade-up">
        <div className="container-fluid">
        
        <Slider {...settings}>
        {this.state.dests.map(dest =>
      <div className="item" key={'dest-${dest.id}}'} >
        <img src={dest.acf.image} className="dest-img img-fluid" />
        <div className="dst-cntnt">
           <h3 className="dst-heading">{dest.acf.title}</h3>
           <p className="dst-pera">{dest.acf.short_discription}</p>
           <p className="dest-dn-pera">{dest.acf.suit}</p>
     </div>
     </div>
      )}

     </Slider>

            
        </div>
        </div>
      
       
      </div>

      )
    }
  }


export default Frstsec;





