import React, { Component } from "react";


class Choseheading extends Component{
    constructor (props){
        super(props);
      
        this.state = {
          acf: {},
        };
      }
      componentDidMount() {
        return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/84`)
        .then((response) => response.json())
        .then((responseJson) => {
          const { acf } =  responseJson;
          this.setState({ acf });       
        })
        .catch((error) => {
          console.error(error);
        });  
    }   
    render(){
        return(
            <div>
            <h4 className="why-pera">{this.state.acf.choose_heading}</h4>
            <h3 className="why-heading">{this.state.acf.choose_sub_heading}</h3>
            </div>
        )
    }
}
export default Choseheading;