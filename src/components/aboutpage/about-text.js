import React, { Component } from "react";
import Choseheading from './about-chose.js'
import First from './chosesec/first.js';
import Second from './chosesec/second.js';
import Third from './chosesec/third.js';
import Four from './chosesec/four.js';




const TITLE = 'ABOUT US'

class Aboutpagesec extends Component{
    constructor (props){
        super(props);
      
        this.state = {
          acf: {},
        };
      }
      componentDidMount() {
        return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/180`)
        .then((response) => response.json())
        .then((responseJson) => {
          const { acf } =  responseJson;
          this.setState({ acf });       
        })
        .catch((error) => {
          console.error(error);
        });  
    }
    render(){
        return(
<div>
<div className="inner-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
      </div>

      <div className="why-sec mt-5">
         <div className="container text-center">
             <Choseheading />
            <div className="row">
                <First />
                <Second />
                <Third />
                <Four />
      </div>

      <div className="about-bar">
      
          <div className="row">
              <div className="col-md-6 col-12"> 
              <div className="about-pera">
          <p>{this.state.acf.text}</p>
              </div>
              </div>
               
              <div className="col-md-6 col-12">
               <div className="about-img">
                     <img src={this.state.acf.image} className="img-fluid" /> 
              </div>
              </div>

            
          </div>
        
           
      </div>
      </div>
    </div>



</div>
        )
    }
}

export default Aboutpagesec;