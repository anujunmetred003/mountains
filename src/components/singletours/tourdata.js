import React from 'react';


const tourData = [
    {
      
      id: 1,
      tourname: "SHIMLA",
      tour:[
          { 
    id: 1.1,          
    urlname: "SHIMLA-MANALI-3N/4D" ,         
    alltourimg: "./images/shimla1.png",
    alltourtitle: "SHIMLA - MANALI 3N/4D",
    alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
    shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
    carouselitems: [
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      }
    ],
    cabname: [
        {
          h2: "Shimla-Manali 3N/4D"
        }
    ],
    carrating: [
      {
          h1: "97%",
          p: "overflow-x guest recomended"
      },
      {
          h1: "4.5/5",
          p: "guest rating"
      }
    ],
    cartabs:[
      {
          list: "ABOUT"
      },
      {
          list: "FAQ'S"
      },
      {
          list: "AMENITIES"
      },
      {
          list: "REVIEWS"
      }
    ],
    tabcontent: [
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
      }
    ],
    assistancedata: [
      {
          assisimg: "../images/call.png",
          span: "Need Assistance?",
          p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
          tel: "1-234-567-8900",
          phone: "tel:1-234-567-8900"
      }
    ],
    morecabs: [
      {
          locimg: "../images/location.png",
          span: "You May Also Like"
      }
    ],
    morecabtext:[
      {
          morecabimg: "../images/cab-6.jpg",
          h5: "Alto",
          p:  "INR 2000",
          url: "/aulto"
      },
      {
          morecabimg: "../images/cab-7.jpg",
          h5: "Wagon- R",
          p:  "INR 200",
          url: "#"
      },
      {
          morecabimg: "../images/cab-4.jpg",
          h5: "Swift-Desire",
          p:  "INR 2000",
          url: "#"
      },
      {
          morecabimg: "../images/cab-3.jpg",
          h5: "Ertiga",
          p:  "INR 2000",
          url: "/aulto"
      }
    ]
                  
},
{
    id: 1.2,
    urlname: "SHIMLA-MANALI-3N/4D", 
      alltourimg: "./images/shilma2.png",
      alltourtitle: "SHIMLA - MANALI 3N/4D",
      alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
      shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
id: 1.3,
urlname: "SHIMLA-MANALI-3N/4D",     
alltourimg: "./images/shimla3.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 1.4,    
urlname: "SHIMLA-MANALI-3N/4D" ,     
alltourimg: "./images/shimla4.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 1.5,
urlname: "SHIMLA-MANALI-3N/4D",     
alltourimg: "./images/shimla1.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
}
      ]
    },  
    {
      id: 2,
      tourname:"MANALI",
      tour: [
          {
            id: 2.1,
    urlname: "SHIMLA-MANALI-3N/4D",           
    alltourimg: "./images/shimla1.png",
    alltourtitle: "SHIMLA - MANALI 3N/4D",
    alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
    shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
    carouselitems: [
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      }
    ],
    cabname: [
        {
          h2: "Shimla-Manali 3N/4D"
        }
    ],
    carrating: [
      {
          h1: "97%",
          p: "overflow-x guest recomended"
      },
      {
          h1: "4.5/5",
          p: "guest rating"
      }
    ],
    cartabs:[
      {
          list: "ABOUT"
      },
      {
          list: "FAQ'S"
      },
      {
          list: "AMENITIES"
      },
      {
          list: "REVIEWS"
      }
    ],
    tabcontent: [
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
      }
    ],
    assistancedata: [
      {
          assisimg: "../images/call.png",
          span: "Need Assistance?",
          p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
          tel: "1-234-567-8900",
          phone: "tel:1-234-567-8900"
      }
    ],
    morecabs: [
      {
          locimg: "../images/location.png",
          span: "You May Also Like"
      }
    ],
    morecabtext:[
      {
          morecabimg: "../images/cab-6.jpg",
          h5: "Alto",
          p:  "INR 2000",
          url: "/aulto"
      },
      {
          morecabimg: "../images/cab-7.jpg",
          h5: "Wagon- R",
          p:  "INR 200",
          url: "#"
      },
      {
          morecabimg: "../images/cab-4.jpg",
          h5: "Swift-Desire",
          p:  "INR 2000",
          url: "#"
      },
      {
          morecabimg: "../images/cab-3.jpg",
          h5: "Ertiga",
          p:  "INR 2000",
          url: "/aulto"
      }
    ]
                  
  },
 {
    id: 2.2,
      urlname: "SHIMLA-MANALI-3N/4D" ,     
      alltourimg: "./images/shilma2.png",
      alltourtitle: "SHIMLA - MANALI 3N/4D",
      alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
      shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 2.3,
urlname: "SHIMLA-MANALI-3N/4D", 
alltourimg: "./images/shimla3.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 2.4,
urlname: "SHIMLA-MANALI-3N/4D",
alltourimg: "./images/shimla4.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 2.5,
    urlname: "SHIMLA-MANALI-3N/4D",
alltourimg: "./images/shimla1.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
}
      ]
    },
    {
      id: 3,
      tourname: "DHARAMSHALA",
      tour: [
          { 
            id: 3.1,
            urlname: "SHIMLA-MANALI-3N/4D",     
    alltourimg: "./images/shimla1.png",
    alltourtitle: "SHIMLA - MANALI 3N/4D",
    alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
    shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
    carouselitems: [
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      }
    ],
    cabname: [
        {
          h2: "Shimla-Manali 3N/4D"
        }
    ],
    carrating: [
      {
          h1: "97%",
          p: "overflow-x guest recomended"
      },
      {
          h1: "4.5/5",
          p: "guest rating"
      }
    ],
    cartabs:[
      {
          list: "ABOUT"
      },
      {
          list: "FAQ'S"
      },
      {
          list: "AMENITIES"
      },
      {
          list: "REVIEWS"
      }
    ],
    tabcontent: [
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
      }
    ],
    assistancedata: [
      {
          assisimg: "../images/call.png",
          span: "Need Assistance?",
          p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
          tel: "1-234-567-8900",
          phone: "tel:1-234-567-8900"
      }
    ],
    morecabs: [
      {
          locimg: "../images/location.png",
          span: "You May Also Like"
      }
    ],
    morecabtext:[
      {
          morecabimg: "../images/cab-6.jpg",
          h5: "Alto",
          p:  "INR 2000",
          url: "/aulto"
      },
      {
          morecabimg: "../images/cab-7.jpg",
          h5: "Wagon- R",
          p:  "INR 200",
          url: "#"
      },
      {
          morecabimg: "../images/cab-4.jpg",
          h5: "Swift-Desire",
          p:  "INR 2000",
          url: "#"
      },
      {
          morecabimg: "../images/cab-3.jpg",
          h5: "Ertiga",
          p:  "INR 2000",
          url: "/aulto"
      }
    ]
                  
  },
 {
    id: 3.2,
    urlname: "SHIMLA-MANALI-3N/4D",
      alltourimg: "./images/shilma2.png",
      alltourtitle: "SHIMLA - MANALI 3N/4D",
      alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
      shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 3.3,
    urlname: "SHIMLA-MANALI-3N/4D",
alltourimg: "./images/shimla3.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 3.4,
    urlname: "SHIMLA-MANALI-3N/4D",
alltourimg: "./images/shimla4.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 3.5,
    urlname: "SHIMLA-MANALI-3N/4D",
alltourimg: "./images/shimla1.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
}
]
    },
    {
      id: 4,
      tourname: "PALAMPUR",
      tour: [
          {
            id: 4.1,
    urlname: "SHIMLA-MANALI-3N/4D",
    alltourimg: "./images/shimla1.png",
    alltourtitle: "SHIMLA - MANALI 3N/4D",
    alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
    shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
    carouselitems: [
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      },
      {
          mainimage: "../images/tour-banner.jpg"
      }
    ],
    cabname: [
        {
          h2: "Shimla-Manali 3N/4D"
        }
    ],
    carrating: [
      {
          h1: "97%",
          p: "overflow-x guest recomended"
      },
      {
          h1: "4.5/5",
          p: "guest rating"
      }
    ],
    cartabs:[
      {
          list: "ABOUT"
      },
      {
          list: "FAQ'S"
      },
      {
          list: "AMENITIES"
      },
      {
          list: "REVIEWS"
      }
    ],
    tabcontent: [
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
      },
      {
          pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
      }
    ],
    assistancedata: [
      {
          assisimg: "../images/call.png",
          span: "Need Assistance?",
          p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
          tel: "1-234-567-8900",
          phone: "tel:1-234-567-8900"
      }
    ],
    morecabs: [
      {
          locimg: "../images/location.png",
          span: "You May Also Like"
      }
    ],
    morecabtext:[
      {
          morecabimg: "../images/cab-6.jpg",
          h5: "Alto",
          p:  "INR 2000",
          url: "/aulto"
      },
      {
          morecabimg: "../images/cab-7.jpg",
          h5: "Wagon- R",
          p:  "INR 200",
          url: "#"
      },
      {
          morecabimg: "../images/cab-4.jpg",
          h5: "Swift-Desire",
          p:  "INR 2000",
          url: "#"
      },
      {
          morecabimg: "../images/cab-3.jpg",
          h5: "Ertiga",
          p:  "INR 2000",
          url: "/aulto"
      }
    ]
                  
  },
{
    id: 4.2,
    urlname: "SHIMLA-MANALI-3N/4D",
      alltourimg: "./images/shilma2.png",
      alltourtitle: "SHIMLA - MANALI 3N/4D",
      alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
      shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 4.3,
    urlname: "SHIMLA-MANALI-3N/4D",
alltourimg: "./images/shimla3.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 4.4,
    urlname: "SHIMLA-MANALI-3N/4D",
alltourimg: "./images/shimla4.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
},
{
    id: 4.5,
    urlname: "SHIMLA-MANALI-3N/4D",
alltourimg: "./images/shimla1.png",
alltourtitle: "SHIMLA - MANALI 3N/4D",
alltourdiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy sometimes known, is dummy",
shimlalongdiscr: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.",
carouselitems: [
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
},
{
    mainimage: "../images/tour-banner.jpg"
}
],
cabname: [
  {
    h2: "Shimla-Manali 3N/4D"
  }
],
carrating: [
{
    h1: "97%",
    p: "overflow-x guest recomended"
},
{
    h1: "4.5/5",
    p: "guest rating"
}
],
cartabs:[
{
    list: "ABOUT"
},
{
    list: "FAQ'S"
},
{
    list: "AMENITIES"
},
{
    list: "REVIEWS"
}
],
tabcontent: [
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
},
{
    pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
}
],
assistancedata: [
{
    assisimg: "../images/call.png",
    span: "Need Assistance?",
    p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
    tel: "1-234-567-8900",
    phone: "tel:1-234-567-8900"
}
],
morecabs: [
{
    locimg: "../images/location.png",
    span: "You May Also Like"
}
],
morecabtext:[
{
    morecabimg: "../images/cab-6.jpg",
    h5: "Alto",
    p:  "INR 2000",
    url: "/aulto"
},
{
    morecabimg: "../images/cab-7.jpg",
    h5: "Wagon- R",
    p:  "INR 200",
    url: "#"
},
{
    morecabimg: "../images/cab-4.jpg",
    h5: "Swift-Desire",
    p:  "INR 2000",
    url: "#"
},
{
    morecabimg: "../images/cab-3.jpg",
    h5: "Ertiga",
    p:  "INR 2000",
    url: "/aulto"
}
]
            
}
      ]
  }
  
];

  export default tourData;

