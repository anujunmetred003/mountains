import React, { Component } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Link, Route } from 'react-router-dom';

const tourURl = 'https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/tours?_embed'

const settings = {
    dots: false,
    infinite: false,
    autoplay: true,
    arrows: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5,
          margin: 10,
          infinite: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
    };
    
const Tourslider = ({ match }) => {
  const categories = match.params.categories;
  const tours = tourURl.find(a => a.categories == categories);
        return(
            <div>
            <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
            <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <Slider {...settings} className="tour_slide_sec position-relative">
            {tours.map((tourtext, key) => (
              <div className="item brdr">
              <img src={tourtext.acf.image} className="tour-img img-fluid" />
              <div className="tour-cntnt">
                <h5 className="tours-heading">{tourtext.acf.tour_name}</h5>
                <p className="tour-pera">{tourtext.acf.about_tour}</p>
                <div className="tour-pg-btn">
                  <Link to={`/tour/${tourtext.slug}`} className="btn tour-bok-btn">BOOK NOW</Link>
                </div>
                  </div>
              </div>
            ))}
            </Slider>
            </div>
         </div>
        
         </div>
        )
    }

export default Tourslider;