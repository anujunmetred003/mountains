import React, { Component } from 'react';
import tourData from './tourdata.js';



class Relatedcabs extends Component {
    
    render(){
    
    const Relate = ({ match }) => {
        const id = match.params.id;
        const cab = tourData.find(cab => cab.id == id);

    return(
    <div>
        {cab.morecabtext.map((more, key) => (
            <a href={`/tour/${more.id}`}>
           <div className="ww">
              <img className="img_icons" src={more.morecabimg} />
              <h5>{more.h5}</h5>
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <p>{more.p}</p>
           </div>
           </a>  
        ))}
      </div>
        )
        }

        return(
            <div>
                { Relate() }
            </div>
        )
        }
}

export default Relatedcabs;