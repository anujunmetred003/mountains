
import React, { Component } from "react";
import Tourslider from './tour-slider.js'
import { Route } from 'react-router-dom';

const TITLE = 'TOURS'

class Tourpagesec extends Component{
  constructor() {
    super();
    this.state = {
      tour: []
    }
  }

  componentDidMount() {
    let dataURL = "https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/categories?_embed";
    fetch(dataURL)
      .then(res => res.json())
      .then(res => {
        this.setState({
          tour: res
        })
      })
  }
render(){
  return(
    <div>
    <div className="inner-cab-banner">
              <div className="container text-center">
                  <h1 className="inner-heading">{ TITLE }</h1>
              </div>
    </div>
    <div className="single-tour">
    <div className="container-fluid">
        <div className="row">
          {this.state.tour.map((tours) => 
            <div key={'tours-${tours.id}}'} className="col-12 position-relative mb-4">
             <nav>
            <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
          <a className="nav-item nav-link active tb-link" id="nav-home-tab" data-toggle="tab" role="tab" aria-controls="nav-home" aria-selected="true">{tours.name}</a>
            </div>
            </nav>
          <Route path="/tour/:categories" render={(props) => <Tourslider /> } />
          {/* <Tourslider /> */}
            </div>
          )} 
            </div>
        </div>
    </div>
    
    </div>
  )
}
}
export default Tourpagesec;

