import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import hotelData from './hoteldata.js';
import NotFound from '../notfound.js';
import Relatedcabs from './relatedcabs.js';

const TITLE = 'CHOOSE YOUR PERFECT STAY'

const Hotels = ({ match }) => {
   
   const id = match.params.id;
   const hotel = hotelData.find(hotel => hotel.id == id);


   if(!hotel) return <NotFound />

   const otherhotel = hotelData.filter(hotel => hotel.id !== id);

  return (
           <div>
         <div className="inner-banner">
         <div className="container text-center">
             <h1 className="inner-heading">{ TITLE }</h1>
         </div>
       </div>

         <div className="first_cab_sec">
         <div className="container cabs">
         <div className="row">        
                     
            <div className="col-12 col-md-8" id="slider"> 
            <Carousel>
            {hotel.carouselitems.map((insidevehcile, key) => (
                     <div className="cabitem">	
                        <img src={insidevehcile.mainimage} className="img-fluid" />
                     </div>
            ))}
            </Carousel> 
            </div>
            
            <div className="col-12 col-md-4">
            {hotel.cabname.map((name, index) => (
               <h2>{name.h2}</h2>
            ))}
               <div className="rating">
                  <span>&#9733;</span> 
                  <span>&#9733;</span> 
                  <span>&#9733;</span> 
                  <span>&#9733;</span> 
                  <span>&#9733;</span> 
               </div>
               <div className="col-md-12"> 
                  <div className="row">
                  {hotel.carrating.map((rates, index) => (
                     <div className="col-md-6 boder">
                        <h1>{rates.h1}</h1>
                        <p>{rates.p}</p>
                     </div>
                   ))}  
                  </div>
               </div>
               <div className="col-md-12 reviews">
                  <div className="row">
                     <div className="col-md-6 col-6"> 
                        <i className="fa fa-user" aria-hidden="true"> 20 Reviews</i>		 
                     </div>
                     <div className="col-md-6 col-6">
                        <a href="#"><p>+ Add Reviews</p></a> 
                     </div> 
                  </div>
               </div>
               <hr />
               <div className="col-md-12 wish">			
                  <a href="#" className="btn"><span>&#9829;</span>
                  ADD TO WISHLIST</a>	 
               </div>
               <div className="col-md-12 wish">		 
                  <a href="#" className="btn">BOOK NOW</a>	 
               </div>
            </div>
         </div> 
      </div>
      <div className="container cabs">
         <div className="row"> 
            <div className="col-md-8">
               <div class="shadow bg-white tab_bar">
                  <Tabs>
                     <TabList>
                     {hotel.cartabs.map((tab, index) => (
                        <Tab>{tab.list}</Tab>
                      ))}    
                     </TabList>

                     {hotel.tabcontent.map((content, index) => (
                     <TabPanel>{content.pera}</TabPanel>
                      ))}
                  </Tabs>
               </div>
            </div>
            <div className="col-md-4">
               <div className="shadow p-3 mb-5 bg-white asis">
               {hotel.assistancedata.map((assis, index) => (
                  <div>
                  <img className="img_call" src={assis.assisimg} />
               <span className="need">{assis.span}</span>
               <p>{assis.p}</p>
               <a href={assis.phone}><h5>{assis.tel}</h5></a>
                  </div>
               ))}   
            </div>
         </div>
      </div>
      </div>
      <div className="container py-4 cabs">
         <div className="row">
            <div className="col-md-8"></div>
            <div className="col-md-4 cccc">
               <div className="shadow p-3 mb-5 bg-white rounded">
                  <div className="location">
                  {hotel.morecabs.map((morettl, index) => (
                     <div>
                     <img className="img_call" src={morettl.locimg} />
                     <span className="need">{morettl.span}</span>
                     </div>
                  ))} 
                  </div>
        <Relatedcabs />
               </div>
            </div>
         </div>
      </div>
      </div>
      </div>
  );
}

export default Hotels;