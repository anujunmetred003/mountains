import React, { Component } from 'react';
import hotelData from './hoteldata.js';
import ReactShadowScroll from 'react-shadow-scroll';



class Relatedcabs extends Component {
    
    render(){
    
    const Relate = () => {
        // const id = match.params.id;
        // const cab = cabData.find(cab => cab.id == id);

    return(
    <div className="scrol_sec">
        <ReactShadowScroll style={{ height: 350 }} id="srcol">
        {hotelData.map(more => 
            <a href={`/hotel/${more.id}`}>
           <div className="ww">
              <img className="img_icons" src={more.hotelimg} />
              <h5>{more.hoteltitle}</h5>
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
           </div>
           </a>  
        )}
        </ReactShadowScroll>
      </div>
        )
        }

        return(
            <div>
                { Relate() }
            </div>
        )
        }
}

export default Relatedcabs;