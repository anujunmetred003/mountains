import React from 'react';


const hotelData = [
    {
      id: 1,
      hotelimg: "../images/room1.png",
      hoteltitle: "HOTEL NAME",
      hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
      carouselitems: [
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        }
      ],
      cabname: [
          {
            h2: "HOTEL NAME"
          }
      ],
      carrating: [
        {
            h1: "97%",
            p: "overflow-x guest recomended"
        },
        {
            h1: "4.5/5",
            p: "guest rating"
        }
      ],
      cartabs:[
        {
            list: "ABOUT"
        },
        {
            list: "FAQ'S"
        },
        {
            list: "AMENITIES"
        },
        {
            list: "REVIEWS"
        }
      ],
      tabcontent: [
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
        }
      ],
      assistancedata: [
        {
            assisimg: "../images/call.png",
            span: "Need Assistance?",
            p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
            tel: "1-234-567-8900",
            phone: "tel:1-234-567-8900"
        }
      ],
      morecabs: [
        {
            locimg: "../images/location.png",
            span: "You May Also Like"
        }
      ],
      morecabtext:[
        {
            morecabimg: "../images/cab-6.jpg",
            h5: "Alto",
            p:  "INR 2000",
            url: "/aulto"
        },
        {
            morecabimg: "../images/cab-7.jpg",
            h5: "Wagon- R",
            p:  "INR 200",
            url: "#"
        },
        {
            morecabimg: "../images/cab-4.jpg",
            h5: "Swift-Desire",
            p:  "INR 2000",
            url: "#"
        },
        {
            morecabimg: "../images/cab-3.jpg",
            h5: "Ertiga",
            p:  "INR 2000",
            url: "/aulto"
        }
      ]
    },  
    {
      id: 2,
      hotelimg: "../images/room2.png",
      hoteltitle: "HOTEL NAME",
      hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
      carouselitems:[
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        }
      ],
      cabname: [
          {
            h2: "HOTEL NAME"
          }
      ],
      carrating: [
        {
            h1: "97%",
            p: "overflow-x guest recomended"
        },
        {
            h1: "4.5/5",
            p: "guest rating"
        }
      ],
      cartabs:[
        {
            list: "ABOUT"
        },
        {
            list: "FAQ'S"
        },
        {
            list: "AMENITIES"
        },
        {
            list: "REVIEWS"
        }
      ],
      tabcontent: [
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
        }
      ],
      assistancedata: [
        {
            assisimg: "../images/call.png",
            span: "Need Assistance?",
            p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
            tel: "1-234-567-8900",
            phone: "tel:1-234-567-8900"
        }
      ],
      morecabs: [
        {
            locimg: "../images/location.png",
            span: "You May Also Like"
        }
      ],
      morecabtext:[
        {
            morecabimg: "../images/cab-6.jpg",
            h5: "Alto",
            p:  "INR 2000",
            url: "/aulto"
        },
        {
            morecabimg: "../images/cab-7.jpg",
            h5: "Wagon- R",
            p:  "INR 200",
            url: "#"
        },
        {
            morecabimg: "../images/cab-4.jpg",
            h5: "Swift-Desire",
            p:  "INR 2000",
            url: "#"
        },
        {
            morecabimg: "../images/cab-3.jpg",
            h5: "Ertiga",
            p:  "INR 2000",
            url: "/aulto"
        }
      ]
    },
    {
      id: 3,
      hotelimg: "../images/room3.png",
      hoteltitle: "HOTEL NAME",
      hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
      carouselitems:[
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        }
      ],
      cabname: [
          {
            h2: "HOTEL NAME"
          }
      ],
      carrating: [
        {
            h1: "97%",
            p: "overflow-x guest recomended"
        },
        {
            h1: "4.5/5",
            p: "guest rating"
        }
      ],
      cartabs:[
        {
            list: "ABOUT"
        },
        {
            list: "FAQ'S"
        },
        {
            list: "AMENITIES"
        },
        {
            list: "REVIEWS"
        }
      ],
      tabcontent: [
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
        }
      ],
      assistancedata: [
        {
            assisimg: "../images/call.png",
            span: "Need Assistance?",
            p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
            tel: "1-234-567-8900",
            phone: "tel:1-234-567-8900"
        }
      ],
      morecabs: [
        {
            locimg: "../images/location.png",
            span: "You May Also Like"
        }
      ],
      morecabtext:[
        {
            morecabimg: "../images/cab-6.jpg",
            h5: "Alto",
            p:  "INR 2000",
            url: "/aulto"
        },
        {
            morecabimg: "../images/cab-7.jpg",
            h5: "Wagon- R",
            p:  "INR 200",
            url: "#"
        },
        {
            morecabimg: "../images/cab-4.jpg",
            h5: "Swift-Desire",
            p:  "INR 2000",
            url: "#"
        },
        {
            morecabimg: "../images/cab-3.jpg",
            h5: "Ertiga",
            p:  "INR 2000",
            url: "/aulto"
        }
      ]
    },
    {
      id: 4,
      hotelimg: "../images/room4.png",
      hoteltitle: "HOTEL NAME",
      hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
      carouselitems:[
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        },
        {
            mainimage: "../images/hotel-banner.jpg"
        }
      ],
      cabname: [
          {
            h2: "HOTEL NAME"
          }
      ],
      carrating: [
        {
            h1: "97%",
            p: "overflow-x guest recomended"
        },
        {
            h1: "4.5/5",
            p: "guest rating"
        }
      ],
      cartabs:[
        {
            list: "ABOUT"
        },
        {
            list: "FAQ'S"
        },
        {
            list: "AMENITIES"
        },
        {
            list: "REVIEWS"
        }
      ],
      tabcontent: [
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
        }
      ],
      assistancedata: [
        {
            assisimg: "../images/call.png",
            span: "Need Assistance?",
            p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
            tel: "1-234-567-8900",
            phone: "tel:1-234-567-8900"
        }
      ],
      morecabs: [
        {
            locimg: "../images/location.png",
            span: "You May Also Like"
        }
      ],
      morecabtext:[
        {
            morecabimg: "../images/cab-6.jpg",
            h5: "Alto",
            p:  "INR 2000",
            url: "/aulto"
        },
        {
            morecabimg: "../images/cab-7.jpg",
            h5: "Wagon- R",
            p:  "INR 200",
            url: "#"
        },
        {
            morecabimg: "../images/cab-4.jpg",
            h5: "Swift-Desire",
            p:  "INR 2000",
            url: "#"
        },
        {
            morecabimg: "../images/cab-3.jpg",
            h5: "Ertiga",
            p:  "INR 2000",
            url: "/aulto"
        }
      ]
    },
    {
        id: 5,
        hotelimg: "../images/room1.png",
        hoteltitle: "HOTEL NAME",
        hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
        carouselitems:[
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            }
        ],
        cabname: [
            {
              h2: "HOTEL NAME"
            }
        ],
        carrating: [
          {
              h1: "97%",
              p: "overflow-x guest recomended"
          },
          {
              h1: "4.5/5",
              p: "guest rating"
          }
        ],
        cartabs:[
          {
              list: "ABOUT"
          },
          {
              list: "FAQ'S"
          },
          {
              list: "AMENITIES"
          },
          {
              list: "REVIEWS"
          }
        ],
        tabcontent: [
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
          }
        ],
        assistancedata: [
          {
              assisimg: "../images/call.png",
              span: "Need Assistance?",
              p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
              tel: "1-234-567-8900",
              phone: "tel:1-234-567-8900"
          }
        ],
        morecabs: [
          {
              locimg: "../images/location.png",
              span: "You May Also Like"
          }
        ],
        morecabtext:[
          {
              morecabimg: "../images/cab-6.jpg",
              h5: "Alto",
              p:  "INR 2000",
              url: "/aulto"
          },
          {
              morecabimg: "../images/cab-7.jpg",
              h5: "Wagon- R",
              p:  "INR 200",
              url: "#"
          },
          {
              morecabimg: "../images/cab-4.jpg",
              h5: "Swift-Desire",
              p:  "INR 2000",
              url: "#"
          },
          {
              morecabimg: "../images/cab-3.jpg",
              h5: "Ertiga",
              p:  "INR 2000",
              url: "/aulto"
          }
        ]
      },
      {
        id: 6,
        hotelimg: "../images/room3.png",
        hoteltitle: "HOTEL NAME",
        hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
        carouselitems:[
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            }
        ],
        cabname: [
            {
              h2: "HOTEL NAME"
            }
        ],
        carrating: [
          {
              h1: "97%",
              p: "overflow-x guest recomended"
          },
          {
              h1: "4.5/5",
              p: "guest rating"
          }
        ],
        cartabs:[
          {
              list: "ABOUT"
          },
          {
              list: "FAQ'S"
          },
          {
              list: "AMENITIES"
          },
          {
              list: "REVIEWS"
          }
        ],
        tabcontent: [
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
          }
        ],
        assistancedata: [
          {
              assisimg: "../images/call.png",
              span: "Need Assistance?",
              p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
              tel: "1-234-567-8900",
              phone: "tel:1-234-567-8900"
          }
        ],
        morecabs: [
          {
              locimg: "../images/location.png",
              span: "You May Also Like"
          }
        ],
        morecabtext:[
          {
              morecabimg: "../images/cab-6.jpg",
              h5: "Alto",
              p:  "INR 2000",
              url: "/aulto"
          },
          {
              morecabimg: "../images/cab-7.jpg",
              h5: "Wagon- R",
              p:  "INR 200",
              url: "#"
          },
          {
              morecabimg: "../images/cab-4.jpg",
              h5: "Swift-Desire",
              p:  "INR 2000",
              url: "#"
          },
          {
              morecabimg: "../images/cab-3.jpg",
              h5: "Ertiga",
              p:  "INR 2000",
              url: "/aulto"
          }
        ]
      },
      {
        id: 7,
        hotelimg: "../images/room4.png",
        hoteltitle: "HOTEL NAME",
        hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
        carouselitems:[
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            }
        ],
        cabname: [
            {
              h2: "HOTEL NAME"
            }
        ],
        carrating: [
          {
              h1: "97%",
              p: "overflow-x guest recomended"
          },
          {
              h1: "4.5/5",
              p: "guest rating"
          }
        ],
        cartabs:[
          {
              list: "ABOUT"
          },
          {
              list: "FAQ'S"
          },
          {
              list: "AMENITIES"
          },
          {
              list: "REVIEWS"
          }
        ],
        tabcontent: [
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
          }
        ],
        assistancedata: [
          {
              assisimg: "../images/call.png",
              span: "Need Assistance?",
              p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
              tel: "1-234-567-8900",
              phone: "tel:1-234-567-8900"
          }
        ],
        morecabs: [
          {
              locimg: "../images/location.png",
              span: "You May Also Like"
          }
        ],
        morecabtext:[
          {
              morecabimg: "../images/cab-6.jpg",
              h5: "Alto",
              p:  "INR 2000",
              url: "/aulto"
          },
          {
              morecabimg: "../images/cab-7.jpg",
              h5: "Wagon- R",
              p:  "INR 200",
              url: "#"
          },
          {
              morecabimg: "../images/cab-4.jpg",
              h5: "Swift-Desire",
              p:  "INR 2000",
              url: "#"
          },
          {
              morecabimg: "../images/cab-3.jpg",
              h5: "Ertiga",
              p:  "INR 2000",
              url: "/aulto"
          }
        ]
      },
      {
        id: 8,
        hotelimg: "../images/room1.png",
        hoteltitle: "HOTEL NAME",
        hoteldiscription: "Lorem ipsum, or lipsum as it is sometimes known, is dummy",
        carouselitems:[
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            },
            {
                mainimage: "../images/hotel-banner.jpg"
            }
        ],
        cabname: [
            {
              h2: "HOTEL NAME"
            }
        ],
        carrating: [
          {
              h1: "97%",
              p: "overflow-x guest recomended"
          },
          {
              h1: "4.5/5",
              p: "guest rating"
          }
        ],
        cartabs:[
          {
              list: "ABOUT"
          },
          {
              list: "FAQ'S"
          },
          {
              list: "AMENITIES"
          },
          {
              list: "REVIEWS"
          }
        ],
        tabcontent: [
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
          }
        ],
        assistancedata: [
          {
              assisimg: "../images/call.png",
              span: "Need Assistance?",
              p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
              tel: "1-234-567-8900",
              phone: "tel:1-234-567-8900"
          }
        ],
        morecabs: [
          {
              locimg: "../images/location.png",
              span: "You May Also Like"
          }
        ],
        morecabtext:[
          {
              morecabimg: "../images/cab-6.jpg",
              h5: "Alto",
              p:  "INR 2000",
              url: "/aulto"
          },
          {
              morecabimg: "../images/cab-7.jpg",
              h5: "Wagon- R",
              p:  "INR 200",
              url: "#"
          },
          {
              morecabimg: "../images/cab-4.jpg",
              h5: "Swift-Desire",
              p:  "INR 2000",
              url: "#"
          },
          {
              morecabimg: "../images/cab-3.jpg",
              h5: "Ertiga",
              p:  "INR 2000",
              url: "/aulto"
          }
        ]
      }
    
  ];

  export default hotelData;

