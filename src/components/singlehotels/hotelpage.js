
import React, { Component } from "react";
import { Link } from "react-router-dom";


const TITLE = "HOTELS"



class Hotelpagesec extends Component{

  constructor(props){
    super(props);

    this.state = {
      hotels: [],
      dataRoute: 'https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/hotel'
    }
  }

  render(){
    return (
            <div>
<div className="inner-cab-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
</div>
<div className="hotel-pge-sec">
    <div className="container">
        <div className="row">
          {this.state.hotels.map((hotel) => 
        <div key={'hotel-${hotel.id}}'} className="col-md-3 col-12 px-1 mb-3">
     <div className="hotel-bar">
     <img src={hotel.images} class="img-fluid" />
               <div class="htl-cntnt">
               <h3 className="htl-heading">{hotel.name}</h3>
               <p className="htl-pera">{hotel.description}</p>
               <Link to={`/hotel/${hotel.id}`} className="btn htl_book">BOOK NOW</Link>
               </div>
      </div>
      </div>
      )} 
        </div>
    </div>
</div>

    </div>

        )

}

componentDidMount(){
  fetch(this.state.dataRoute)
    .then(res => res.json())
    .then(hotels => this.setState((prevState, props) => {
      return { hotels: hotels.map(this.maphotel)};
    }));
}

maphotel(hotel){
  return {
    id: hotel.id,
    images: hotel.acf.image,
    name: hotel.acf.hotel_name,
    description: hotel.acf.hotel_description
  }
}

}

export default Hotelpagesec;

