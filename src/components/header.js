import React, { Component } from "react";
import { Link } from 'react-router-dom';


class Headersec extends Component{
   constructor (props){
      super(props);
    
      this.state = {
        acf: {},
      };
    }
    componentDidMount() {
      return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/200`)
      .then((response) => response.json())
      .then((responseJson) => {
        const { acf } =  responseJson;
        this.setState({ acf });       
      })
      .catch((error) => {
        console.error(error);
      });  
  }
   render(){
      return(
         <div>
      
         <div className="upr-contact">
        <div className="container upr-bar">
           <div className="row">
              <div className="col-6">
                 <ul className="social-lists">
                    <li className="web-list-item">
                    <a href={`tel:${this.state.acf.phone_no}`} className="web-link"><img src={this.state.acf.phone_icon} className="web-img fadeIn" /><span className="web-no  fadeIn">{this.state.acf.phone_no}</span></a>
                    </li>
                 </ul>
              </div>
              <div className="col-6 social-bar">
                 <ul className="social-lists">
                    <li className="social-list-item">
                       <a href={this.state.acf.insta_url} className="social-list-link"><img src={this.state.acf.insta_icon} className="social-img fadeIn" /></a>
                       <a href={this.state.acf.twitter_url} className="social-list-link"><img src={this.state.acf.twitter_icon} className="social-img fadeIn" /></a>
                       <a href={this.state.acf.facebook_url} className="social-list-link"><img src={this.state.acf.facebook_icon} className="social-fb-img fadeIn" /></a>
                       <a href={this.state.acf.pinterest_url} className="social-list-link"><img src={this.state.acf.pinterest_icon} className="social-img fadeIn" /></a>
                       <a href={this.state.acf.you_tube_url} className="social-list-link"><img src={this.state.acf.you_tube_icon} className="social-img fadeIn" /></a>
                    </li>
                 </ul>
              </div>
           </div>
        </div>
     </div>
   <header id="main-header">
        <div className="container nav-bar">
           <nav className="navbar navbar-expand-lg navbar-light nav-bar nav-header">
              <div className="row w-100 resp">
                 <button className="navbar-toggler toggle collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                 <span className="navbar-toggler-icon"></span>
                 </button>
                 <div className="logo-resp col">
                    <a href='/' className="logo-link"><img src={this.state.acf.logo} className="logo-image" /></a>  
                 </div>
              </div>
              <div className="collapse navbar-collapse back-color" id="navbarSupportedContent">
                 <ul className="navbar-nav mr-auto left-nav">
                    <li className="nav-item menu-list">
                    <Link to='/destinations' className="nav-link menu">
                          <img src={this.state.acf.destination_image} className="menu-img bounceInUp" />
                          <p>{this.state.acf.destination_text}</p>
                      </Link>
                    </li>
                     <li className="nav-item menu-list">
                     <Link to='/hotels' className="nav-link menu">
                           <img src={this.state.acf.hotel_image} className="menu-img bounceInUp" />
                           <p>{this.state.acf.hotel_text}</p>
                       </Link>
                     </li>
                    <li className="nav-item menu-list">
                    <Link to='/vehciles' className="nav-link menu">
                          <img src={this.state.acf.cab_image} className="menu-img bounceInUp" />
                          <p>{this.state.acf.cab_text}</p>
                      </Link>
                    </li>                                          
                 </ul>
                 <div className="logo text-center">
                    <a href='/' className="logo-link"><img src={this.state.acf.logo} className="logo-image" /></a>
                 </div>
                 <ul className="navbar-nav right-nav ml-auto">
                    <li className="nav-item menu-list">
                    <Link to='/tours' className="nav-link menu">
                          <img src={this.state.acf.tour_image} className="menu-img bounceInUp" />
                          <p>{this.state.acf.tour_text}</p>
                      </Link>
                    </li>
                    <li className="nav-item menu-list">
                    <Link to='/about' className="nav-link menu">
                          <img src={this.state.acf.about_image} className="menu-img bounceInUp" />
                          <p>{this.state.acf.about_text}</p>
                      </Link>
                    </li>
                    <li className="nav-item menu-list">
                    <Link to='/contact' className="nav-link menu">
                          <img src={this.state.acf.contact_image} className="menu-img bounceInUp" />
                          <p>{this.state.acf.contact_text}</p>
                      </Link>
                    </li>                                        
                 </ul>
              </div>
           </nav>
        </div>
     </header>
       </div>
      )
   }
}

export default Headersec;