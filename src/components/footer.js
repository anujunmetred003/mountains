import React, { Component } from "react";

class Fotersec extends Component{
   constructor (props){
      super(props);
    
      this.state = {
        acf: {},
      };
    }
    componentDidMount() {
      return fetch(`https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/Pages/244`)
      .then((response) => response.json())
      .then((responseJson) => {
        const { acf } =  responseJson;
        this.setState({ acf });       
      })
      .catch((error) => {
        console.error(error);
      });  
  }  
   render(){
      return(
         <div>
      
         <footer className="foot-sec">
        <div className="container foter-inner">
           <div className="row">
              <div className="col-md-4 col-12">
                 <div className="foot-logo">
                    <a href="/"><img src={this.state.acf.logo} className="foot-logo-img" /></a> 
                 </div>
              </div>
              <div className="col-md-4 col-12">
                 <div className="foot-content text-center">
                    <div className="hr-sect">GET IN TOUCH</div>
                 </div>
                 <ul className="foot-item">
                    <li className="foot-list">
                       <a href="/" className="foot-social"><img src={this.state.acf.twitter_icon} className="fot-logo-img"/></a> 
                       <a href="/" className="foot-social"><img src={this.state.acf.whats_app_icon} className="fot-logo-img" /></a> 
                       <a href="/" className="foot-social"><img src={this.state.acf.facebook_icon} className="fot-logo-img" /></a> 
                       <a href="/" className="foot-social"><img src={this.state.acf.insta_icon} className="fot-logo-img" /></a> 
                    </li>
                 </ul>
              </div>
              <div className="col-md-4 col-12">
                 <div className="hr-sect">NEWSLETTER</div>
                 <div className="col-12">
                    <form action="" className="form-inlin justify-content-center">
                       <div className="form-group">
                          <input type="text" className="form-control fields" placeholder="EMAIL" />
                       </div>
                       <div className="form-group">
                          <input type="submit" value="SUBSCRIBE" className="message-btn btn-block btn-lg" />
                       </div>
                    </form>
                 </div>
              </div>
           </div>
        </div>
     </footer>
     <div className="cpy-sec">
        <div className="container cpy-contain">
           <div className="row">
              <div className="col-md-6 col-12">
                 <p className="cpy-pera"> &#64; 2019 ALL RIGHTS RESERVED <a href="/">MYSTIC MOUNTAINS</a> </p>
              </div>
              <div className="col-md-6 col-12 text-right">
                 <div className="row">
                    <div className="col-6">
                       <p className="pp-pera"><a href="./">PRIVACY POLICIES</a></p>
                    </div>
                    <div className="col-6">
                       <p className="pp-pera"><a href="./">TERMS OF SERVICES</a></p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
     
       </div>
      )
   }
}

export default Fotersec;





