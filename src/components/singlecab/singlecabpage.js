import React, { Component } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
//import cabData from './cabdata.js';
import NotFound from '../notfound.js';
import Relatedcab from './relatedcabs.js';
import axios from 'axios';


const TITLE = 'CHOOSE YOUR PERFECT VEHICLE'

class Cars extends Component{
   constructor() {
		super();

		this.state = {
			loading : false,
			cab: {},
			error: ''
		};
   }
   componentDidMount() {
		console.warn( this.props.id );

		this.setState( { loading: true }, () => {
			axios.get( `https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/cab/${this.props.id}` )
				.then( res => {
					console.warn( res.data );
					if ( Object.keys( res.data ).length ) {
						this.setState( { loading: false, cab: res.data } );
					} //else {
					// 	this.setState( { loading: false, error: 'No Posts Found' } );
					// }
				} )
				.catch( err => this.setState( { loading: false, error: err.response.data.message } ) );
		} )
	}
   render(){
      const { loading, cab, error } = this.state;
         return(
           <div key={cab.id}>
         <div className="inner-banner">
         <div className="container text-center">
             <h1 className="inner-heading">{ TITLE }</h1>
         </div>
       </div>
       <div className="first_cab_sec">
               <div className="container cabs">
               <div className="row">        
                           
                  <div className="col-12 col-md-8" id="slider"> 
                  <Carousel>
                     
                           <div className="cabitem">	
                              <img src={cab.acf.inner_images} className="img-fluid" />
                           </div>
                  
                  </Carousel> 
                  </div>
                  
                  <div className="col-12 col-md-4">
                     <h2>{cab.acf.cab_name}</h2>
                     <div className="rating">
                        <span>&#9733;</span> 
                        <span>&#9733;</span> 
                        <span>&#9733;</span> 
                        <span>&#9733;</span> 
                        <span>&#9733;</span> 
                     </div>
                     <div className="col-md-12"> 
                        <div className="row">
                           <div className="col-md-6 boder">
                              <h1>{cab.acf.guest_recomended}</h1>
                              <p>overflow-x guest recomended</p>
                           </div>
                           <div className="col-md-6 boder">
                              <h1>{cab.acf.guest_rating}</h1>
                              <p>guest rating</p>
                           </div>
                        </div>
                     </div>
                     <div className="col-md-12 reviews">
                        <div className="row">
                           <div className="col-md-6 col-6"> 
                              <i className="fa fa-user" aria-hidden="true"> 20 Reviews</i>		 
                           </div>
                           <div className="col-md-6 col-6">
                              <a href="#"><p>+ Add Reviews</p></a> 
                           </div> 
                        </div>
                     </div>
                     <hr />
                     <div className="col-md-12 wish">			
                        <a href="#" className="btn"><span>&#9829;</span>
                        ADD TO WISHLIST</a>	 
                     </div>
                     <div className="col-md-12 wish">		 
                        <a href="#" className="btn">BOOK NOW</a>	 
                     </div>
                  </div>
               </div> 
            </div>
            <div className="container cabs">
               <div className="row"> 
                  <div className="col-md-8">
                     <div class="shadow bg-white tab_bar">
                        <Tabs>
                           <TabList>
                              <Tab>ABOUT</Tab>
                              <Tab>FAQ'S</Tab>
                              <Tab>AMENITIES</Tab>
                              <Tab>REVIEWS</Tab>   
                           </TabList>
                           <div>
                           <TabPanel>{cab.acf.about}</TabPanel>
                           <TabPanel>{cab.acf.faqs}</TabPanel>
                           <TabPanel>{cab.acf.amenities}</TabPanel>
                           <TabPanel>{cab.acf.reviews}</TabPanel>
                           </div>
                        </Tabs>
                     </div>
                  </div>
                  <div className="col-md-4">
                   <div className="shadow p-3 mb-5 bg-white asis">
                        <div>
                        <img className="img_call" src={cab.acf.assisimg} />
                     <span className="need">{cab.acf.span}</span>
                     <p>{cab.acf.text}</p>
                     <a href={`tel:${cab.acf.phone}`} ><h5>{cab.acf.phone}</h5></a>
                        </div> 
                  </div>
               </div>
            </div>
            </div>
            <div className="container py-4 cabs">
               <div className="row">
                  <div className="col-md-8"></div>
                  <div className="col-md-4 cccc">
                     <div className="shadow p-3 mb-5 bg-white rounded">
                        <div className="location">
                           <div>
                           <img className="img_call" src={cab.acf.loc_img} />
                           <span className="need">{cab.acf.also_like}</span>
                           </div>
                        </div>
              <Relatedcab />
              
                     </div>
                  </div>
               </div>
            </div>
            </div>
      </div>
       );
      
      }
   }

export default Cars;