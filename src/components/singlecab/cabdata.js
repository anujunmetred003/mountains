import React from 'react';


const cabData = [
    {
      id: 1,
      cabimage: "../images/cab-1.jpg",
      name: "INDIGO",
      cabprice: "12000 INR",
      carouselitems: [
        {
            mainimage: "../images/crousal-full-1.jpg"
        },
        {
            mainimage: "../images/crousal-full-5.jpg"
        },
        {
            mainimage: "../images/crousal-full-6.jpg"
        },
        {
            mainimage: "../images/crousal-full-7.jpg"
        },
        {
            mainimage: "../images/crousal-full-8.jpg"
        }
      ],
      cabname: [
          {
            h2: "Indigo"
          }
      ],
      carrating: [
        {
            h1: "97%",
            p: "overflow-x guest recomended"
        },
        {
            h1: "4.5/5",
            p: "guest rating"
        }
      ],
      cartabs:[
        {
            list: "ABOUT"
        },
        {
            list: "FAQ'S"
        },
        {
            list: "AMENITIES"
        },
        {
            list: "REVIEWS"
        }
      ],
      tabcontent: [
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
        }
      ],
      assistancedata: [
        {
            assisimg: "../images/call.png",
            span: "Need Assistance?",
            p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
            tel: "1-234-567-8900",
            phone: "tel:1-234-567-8900"
        }
      ],
      morecabs: [
        {
            locimg: "../images/location.png",
            span: "You May Also Like"
        }
      ],
      morecabtext:[
        {
            morecabimg: "../images/cab-6.jpg",
            h5: "Alto",
            p:  "INR 2000",
            url: "/aulto"
        },
        {
            morecabimg: "../images/cab-7.jpg",
            h5: "Wagon- R",
            p:  "INR 200",
            url: "#"
        },
        {
            morecabimg: "../images/cab-4.jpg",
            h5: "Swift-Desire",
            p:  "INR 2000",
            url: "#"
        },
        {
            morecabimg: "../images/cab-3.jpg",
            h5: "Ertiga",
            p:  "INR 2000",
            url: "/aulto"
        }
      ]
    },  
    {
      id: 2,
      cabimage: "../images/cab-2.jpg",
      name: "INNOVA",
      cabprice: "12000 INR",
      carouselitems:[
        {
            mainimage: "../images/crousal-full-1.jpg"
        },
        {
            mainimage: "../images/crousal-full-5.jpg"
        },
        {
            mainimage: "../images/crousal-full-6.jpg"
        },
        {
            mainimage: "../images/crousal-full-7.jpg"
        },
        {
            mainimage: "../images/crousal-full-8.jpg"
        }
      ],
      cabname: [
          {
            h2: "INNOVA"
          }
      ],
      carrating: [
        {
            h1: "97%",
            p: "overflow-x guest recomended"
        },
        {
            h1: "4.5/5",
            p: "guest rating"
        }
      ],
      cartabs:[
        {
            list: "ABOUT"
        },
        {
            list: "FAQ'S"
        },
        {
            list: "AMENITIES"
        },
        {
            list: "REVIEWS"
        }
      ],
      tabcontent: [
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
        }
      ],
      assistancedata: [
        {
            assisimg: "../images/call.png",
            span: "Need Assistance?",
            p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
            tel: "1-234-567-8900",
            phone: "tel:1-234-567-8900"
        }
      ],
      morecabs: [
        {
            locimg: "../images/location.png",
            span: "You May Also Like"
        }
      ],
      morecabtext:[
        {
            morecabimg: "../images/cab-6.jpg",
            h5: "Alto",
            p:  "INR 2000",
            url: "/aulto"
        },
        {
            morecabimg: "../images/cab-7.jpg",
            h5: "Wagon- R",
            p:  "INR 200",
            url: "#"
        },
        {
            morecabimg: "../images/cab-4.jpg",
            h5: "Swift-Desire",
            p:  "INR 2000",
            url: "#"
        },
        {
            morecabimg: "../images/cab-3.jpg",
            h5: "Ertiga",
            p:  "INR 2000",
            url: "/aulto"
        }
      ]
    },
    {
      id: 3,
      cabimage: "../images/cab-3.jpg",
      name: "ERTICA",
      cabprice: "12000 INR",
      carouselitems:[
        {
            mainimage: "../images/crousal-full-1.jpg"
        },
        {
            mainimage: "../images/crousal-full-5.jpg"
        },
        {
            mainimage: "../images/crousal-full-6.jpg"
        },
        {
            mainimage: "../images/crousal-full-7.jpg"
        },
        {
            mainimage: "../images/crousal-full-8.jpg"
        }
      ],
      cabname: [
          {
            h2: "ERTICA"
          }
      ],
      carrating: [
        {
            h1: "97%",
            p: "overflow-x guest recomended"
        },
        {
            h1: "4.5/5",
            p: "guest rating"
        }
      ],
      cartabs:[
        {
            list: "ABOUT"
        },
        {
            list: "FAQ'S"
        },
        {
            list: "AMENITIES"
        },
        {
            list: "REVIEWS"
        }
      ],
      tabcontent: [
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
        }
      ],
      assistancedata: [
        {
            assisimg: "../images/call.png",
            span: "Need Assistance?",
            p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
            tel: "1-234-567-8900",
            phone: "tel:1-234-567-8900"
        }
      ],
      morecabs: [
        {
            locimg: "../images/location.png",
            span: "You May Also Like"
        }
      ],
      morecabtext:[
        {
            morecabimg: "../images/cab-6.jpg",
            h5: "Alto",
            p:  "INR 2000",
            url: "/aulto"
        },
        {
            morecabimg: "../images/cab-7.jpg",
            h5: "Wagon- R",
            p:  "INR 200",
            url: "#"
        },
        {
            morecabimg: "../images/cab-4.jpg",
            h5: "Swift-Desire",
            p:  "INR 2000",
            url: "#"
        },
        {
            morecabimg: "../images/cab-3.jpg",
            h5: "Ertiga",
            p:  "INR 2000",
            url: "/aulto"
        }
      ]
    },
    {
      id: 4,
      cabimage: "../images/cab-4.jpg",
      name: "SWIFT DESIRE",
      cabprice: "12000 INR",
      carouselitems:[
        {
            mainimage: "../images/crousal-full-1.jpg"
        },
        {
            mainimage: "../images/crousal-full-5.jpg"
        },
        {
            mainimage: "../images/crousal-full-6.jpg"
        },
        {
            mainimage: "../images/crousal-full-7.jpg"
        },
        {
            mainimage: "../images/crousal-full-8.jpg"
        }
      ],
      cabname: [
          {
            h2: "SWIFT DESIRE"
          }
      ],
      carrating: [
        {
            h1: "97%",
            p: "overflow-x guest recomended"
        },
        {
            h1: "4.5/5",
            p: "guest rating"
        }
      ],
      cartabs:[
        {
            list: "ABOUT"
        },
        {
            list: "FAQ'S"
        },
        {
            list: "AMENITIES"
        },
        {
            list: "REVIEWS"
        }
      ],
      tabcontent: [
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        },
        {
            pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
        }
      ],
      assistancedata: [
        {
            assisimg: "../images/call.png",
            span: "Need Assistance?",
            p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
            tel: "1-234-567-8900",
            phone: "tel:1-234-567-8900"
        }
      ],
      morecabs: [
        {
            locimg: "../images/location.png",
            span: "You May Also Like"
        }
      ],
      morecabtext:[
        {
            morecabimg: "../images/cab-6.jpg",
            h5: "Alto",
            p:  "INR 2000",
            url: "/aulto"
        },
        {
            morecabimg: "../images/cab-7.jpg",
            h5: "Wagon- R",
            p:  "INR 200",
            url: "#"
        },
        {
            morecabimg: "../images/cab-4.jpg",
            h5: "Swift-Desire",
            p:  "INR 2000",
            url: "#"
        },
        {
            morecabimg: "../images/cab-3.jpg",
            h5: "Ertiga",
            p:  "INR 2000",
            url: "/aulto"
        }
      ]
    },
    {
        id: 5,
        cabimage: "../images/cab-5.jpg",
        name: "TRAVELLER",
        cabprice: "12000 INR",
        carouselitems:[
          {
              mainimage: "../images/crousal-full-1.jpg"
          },
          {
              mainimage: "../images/crousal-full-5.jpg"
          },
          {
              mainimage: "../images/crousal-full-6.jpg"
          },
          {
              mainimage: "../images/crousal-full-7.jpg"
          },
          {
              mainimage: "../images/crousal-full-8.jpg"
          }
        ],
        cabname: [
            {
              h2: "TRAVELLER"
            }
        ],
        carrating: [
          {
              h1: "97%",
              p: "overflow-x guest recomended"
          },
          {
              h1: "4.5/5",
              p: "guest rating"
          }
        ],
        cartabs:[
          {
              list: "ABOUT"
          },
          {
              list: "FAQ'S"
          },
          {
              list: "AMENITIES"
          },
          {
              list: "REVIEWS"
          }
        ],
        tabcontent: [
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
          }
        ],
        assistancedata: [
          {
              assisimg: "../images/call.png",
              span: "Need Assistance?",
              p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
              tel: "1-234-567-8900",
              phone: "tel:1-234-567-8900"
          }
        ],
        morecabs: [
          {
              locimg: "../images/location.png",
              span: "You May Also Like"
          }
        ],
        morecabtext:[
          {
              morecabimg: "../images/cab-6.jpg",
              h5: "Alto",
              p:  "INR 2000",
              url: "/aulto"
          },
          {
              morecabimg: "../images/cab-7.jpg",
              h5: "Wagon- R",
              p:  "INR 200",
              url: "#"
          },
          {
              morecabimg: "../images/cab-4.jpg",
              h5: "Swift-Desire",
              p:  "INR 2000",
              url: "#"
          },
          {
              morecabimg: "../images/cab-3.jpg",
              h5: "Ertiga",
              p:  "INR 2000",
              url: "/aulto"
          }
        ]
      },
      {
        id: 6,
        cabimage: "../images/cab-6.jpg",
        name: "ALTO 800",
        cabprice: "8000 INR",
        carouselitems:[
          {
              mainimage: "../images/crousal-full-1.jpg"
          },
          {
              mainimage: "../images/crousal-full-5.jpg"
          },
          {
              mainimage: "../images/crousal-full-6.jpg"
          },
          {
              mainimage: "../images/crousal-full-7.jpg"
          },
          {
              mainimage: "../images/crousal-full-8.jpg"
          }
        ],
        cabname: [
            {
              h2: "ALTO 800"
            }
        ],
        carrating: [
          {
              h1: "97%",
              p: "overflow-x guest recomended"
          },
          {
              h1: "4.5/5",
              p: "guest rating"
          }
        ],
        cartabs:[
          {
              list: "ABOUT"
          },
          {
              list: "FAQ'S"
          },
          {
              list: "AMENITIES"
          },
          {
              list: "REVIEWS"
          }
        ],
        tabcontent: [
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
          }
        ],
        assistancedata: [
          {
              assisimg: "../images/call.png",
              span: "Need Assistance?",
              p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
              tel: "1-234-567-8900",
              phone: "tel:1-234-567-8900"
          }
        ],
        morecabs: [
          {
              locimg: "../images/location.png",
              span: "You May Also Like"
          }
        ],
        morecabtext:[
          {
              morecabimg: "../images/cab-6.jpg",
              h5: "Alto",
              p:  "INR 2000",
              url: "/aulto"
          },
          {
              morecabimg: "../images/cab-7.jpg",
              h5: "Wagon- R",
              p:  "INR 200",
              url: "#"
          },
          {
              morecabimg: "../images/cab-4.jpg",
              h5: "Swift-Desire",
              p:  "INR 2000",
              url: "#"
          },
          {
              morecabimg: "../images/cab-3.jpg",
              h5: "Ertiga",
              p:  "INR 2000",
              url: "/aulto"
          }
        ]
      },
      {
        id: 7,
        cabimage: "../images/cab-2.jpg",
        name: "INNOVA",
        cabprice: "12000 INR",
        carouselitems:[
          {
              mainimage: "../images/crousal-full-1.jpg"
          },
          {
              mainimage: "../images/crousal-full-5.jpg"
          },
          {
              mainimage: "../images/crousal-full-6.jpg"
          },
          {
              mainimage: "../images/crousal-full-7.jpg"
          },
          {
              mainimage: "../images/crousal-full-8.jpg"
          }
        ],
        cabname: [
            {
              h2: "INNOVA"
            }
        ],
        carrating: [
          {
              h1: "97%",
              p: "overflow-x guest recomended"
          },
          {
              h1: "4.5/5",
              p: "guest rating"
          }
        ],
        cartabs:[
          {
              list: "ABOUT"
          },
          {
              list: "FAQ'S"
          },
          {
              list: "AMENITIES"
          },
          {
              list: "REVIEWS"
          }
        ],
        tabcontent: [
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
          }
        ],
        assistancedata: [
          {
              assisimg: "../images/call.png",
              span: "Need Assistance?",
              p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
              tel: "1-234-567-8900",
              phone: "tel:1-234-567-8900"
          }
        ],
        morecabs: [
          {
              locimg: "../images/location.png",
              span: "You May Also Like"
          }
        ],
        morecabtext:[
          {
              morecabimg: "../images/cab-6.jpg",
              h5: "Alto",
              p:  "INR 2000",
              url: "/aulto"
          },
          {
              morecabimg: "../images/cab-7.jpg",
              h5: "Wagon- R",
              p:  "INR 200",
              url: "#"
          },
          {
              morecabimg: "../images/cab-4.jpg",
              h5: "Swift-Desire",
              p:  "INR 2000",
              url: "#"
          },
          {
              morecabimg: "../images/cab-3.jpg",
              h5: "Ertiga",
              p:  "INR 2000",
              url: "/aulto"
          }
        ]
      },
      {
        id: 8,
        cabimage: "../images/cab-1.jpg",
        name: "INDIGO",
        cabprice: "12000 INR",
        carouselitems:[
          {
              mainimage: "../images/crousal-full-1.jpg"
          },
          {
              mainimage: "../images/crousal-full-5.jpg"
          },
          {
              mainimage: "../images/crousal-full-6.jpg"
          },
          {
              mainimage: "../images/crousal-full-7.jpg"
          },
          {
              mainimage: "../images/crousal-full-8.jpg"
          }
        ],
        cabname: [
            {
              h2: "INDIGO"
            }
        ],
        carrating: [
          {
              h1: "97%",
              p: "overflow-x guest recomended"
          },
          {
              h1: "4.5/5",
              p: "guest rating"
          }
        ],
        cartabs:[
          {
              list: "ABOUT"
          },
          {
              list: "FAQ'S"
          },
          {
              list: "AMENITIES"
          },
          {
              list: "REVIEWS"
          }
        ],
        tabcontent: [
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop"
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          },
          {
              pera: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing"
          }
        ],
        assistancedata: [
          {
              assisimg: "../images/call.png",
              span: "Need Assistance?",
              p: "our team is 24/7 at your service to help you with your booking issues or answer anyn related questions",
              tel: "1-234-567-8900",
              phone: "tel:1-234-567-8900"
          }
        ],
        morecabs: [
          {
              locimg: "../images/location.png",
              span: "You May Also Like"
          }
        ],
        morecabtext:[
          {
              morecabimg: "../images/cab-6.jpg",
              h5: "Alto",
              p:  "INR 2000",
              url: "/aulto"
          },
          {
              morecabimg: "../images/cab-7.jpg",
              h5: "Wagon- R",
              p:  "INR 200",
              url: "#"
          },
          {
              morecabimg: "../images/cab-4.jpg",
              h5: "Swift-Desire",
              p:  "INR 2000",
              url: "#"
          },
          {
              morecabimg: "../images/cab-3.jpg",
              h5: "Ertiga",
              p:  "INR 2000",
              url: "/aulto"
          }
        ]
      }
    
  ];

  export default cabData;

