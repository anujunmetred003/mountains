
import React, { Component, Fragment } from "react";
//import cabData from './cabdata.js';
import { Link, Route } from "react-router-dom";
import axios from 'axios'
// import Cars from './singlecabpage.js';
// import clientConfig from './client-config';

const TITLE = "Let's find your perfect cab"


class Cabpagesec extends Component{
  // constructor() {
  //   super();
  //   this.state = {
  //     dests: []
  //   }
  // }

  // componentDidMount() {
  //   let dataURL = "https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/destination?_embed";
  //   fetch(dataURL)
  //     .then(res => res.json())
  //     .then(res => {
  //       this.setState({
  //         dests: res
  //       })
  //     })
  // }

	constructor( ) {
		super( );

		this.state = {
			loading : false,
			cabs: [],
			error: ''
		};
	}
  componentDidMount() {

      this.setState( { loading: true }, () => {
        axios.get( `https://wordpress-424831-1397237.cloudwaysapps.com/wp-json/wp/v2/cab` )
          .then( res => {
            if ( res.data.length ) {
              this.setState( { loading: false, cabs: res.data } );
            } else {
              this.setState( { loading: false, error: 'No cab Found' } );
            }
          } )
          .catch( err => this.setState( { loading: false, error: err.response.data.message } ) );
      } )
  }

  render(){
    const { loading, cabs, error } = this.state;
		console.warn( cabs );
    return(
      <div>
<div className="inner-cab-banner">
          <div className="container text-center">
              <h1 className="inner-heading">{ TITLE }</h1>
          </div>
</div>
<div className="hotel-pge-sec">
    <div className="container">
        <div className="row">
        {this.state.cabs.map(cab => 
        <div key={cab.id} className="col-md-3 col-12 px-3 mb-3">
      <div className="cab-bar">
      <img src={cab.acf.cab_image} class="img-fluid" />
                <div class="cab">
                   <h4>{cab.acf.cab_name}</h4>
                   <p>{cab.acf.cab_price}</p> 
                   <Link to={`/vehcile/${cab.id}`} className="btn htl_book">BOOK NOW</Link>
                </div>
       </div>
       </div>
        )}
        </div>
    </div>
</div>
</div>
    )

}
}

export default Cabpagesec;

