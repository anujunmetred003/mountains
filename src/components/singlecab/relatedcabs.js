import React, { Component } from 'react';
import cabData from './cabdata.js';
import ReactShadowScroll from 'react-shadow-scroll';



class Relatedcabs extends Component {
    
    render(){
    
    const Relate = () => {
        // const id = match.params.id;
        // const cab = cabData.find(cab => cab.id == id);

    return(
    <div className="scrol_sec">
        <ReactShadowScroll style={{ height: 350 }} id="srcol">
        {cabData.map(more => 
            <a href={`/vehcile/${more.id}`}>
           <div className="ww">
              <img className="img_cab_icons" src={more.cabimage} />
              <h5>{more.name}</h5>
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <span>&#9733;</span> 
              <p>{more.cabprice}</p> 
           </div>
           </a>  
        )}
        </ReactShadowScroll>
      </div>
        )
        }

        return(
            <div>
                { Relate() }
            </div>
        )
        }
}

export default Relatedcabs;